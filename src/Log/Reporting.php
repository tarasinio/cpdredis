<?php
/**
 * Created by PhpStorm.
 * User: ydmytrunets
 * Date: 8/25/2015
 * Time: 4:43 PM
 */

namespace SebastianBergmann\PHPCPD\Log;

class Reporting
{
    /**
     * Produce the appropriate report object based on $type parameter.
     *
     * @param string $type The type of the report.
     *
     * @return ReportInterface
     * @throws \InvalidArgumentException report is not available.
     */
    public function factory($type)
    {
        $type = ucfirst($type);

        if (class_exists('\\SebastianBergmann\\PHPCPD\\Log\\' . $type, true) === false) {
            echo 'ERROR: Report type "' . $type . '" not found' . PHP_EOL;
            exit(2);
        }

        $className = '\\SebastianBergmann\\PHPCPD\\Log\\' . $type;
        $reportClass = new $className();
        if (false === ($reportClass instanceof ReportInterface)) {
            throw new \InvalidArgumentException(
                'Class "' . $type . '" must implement the "ReportInterface" interface.'
            );
        }

        return $reportClass;
    }

    /**
     * @param string $type
     * @param string $reportFile
     * @param array $clones
     */
    public function printReport($type, $reportFile = '', array $clones)
    {
        $reportClass = $this->factory($type);
        ob_start();
        $reportClass->processClones($clones);
        $generatedReport = ob_get_contents();
        ob_end_clean();

        if ($reportFile !== null) {
            file_put_contents($reportFile, $generatedReport . PHP_EOL);
        }
    }
} 
