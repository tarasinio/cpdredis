<?php
/**
 * Created by PhpStorm.
 * User: ydmytrunets
 * Date: 8/25/2015
 * Time: 3:56 PM
 */

namespace SebastianBergmann\PHPCPD\Log;


class Json implements ReportInterface
{
    /**
     * Processes a list of clones.
     *
     * @param array $clones
     */
    public function processClones(array $clones)
    {
        $duplication = array();
        if (isset($clones['clones'])) {
            foreach ($clones['clones'] as $i => $clone) {

                $duplication['duplication'][$i]['lines'] = $clone['linesCount'];
                $duplication['duplication'][$i]['tokens'] = $clone['tokenCount'];

                foreach ($clone['files'] as $j => $codeCloneFile) {
                    $duplication['duplication'][$i]['file'][$j]['path'] = $codeCloneFile['name'];
                    $duplication['duplication'][$i]['file'][$j]['startLine'] = $codeCloneFile['startLine'];
                }
            }
        }

        echo json_encode(array('pmd-cpd' => $duplication));
    }
} 
