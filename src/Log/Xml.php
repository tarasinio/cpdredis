<?php
/*
 * This file is part of PHP Copy/Paste Detector (PHPCPD).
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\PHPCPD\Log;

use SebastianBergmann\PHPCPD\CodeCloneMap;

/**
 * Implementation of AbstractXmlLogger that writes in PMD-CPD format.
 *
 * @author    Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @license   http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link      http://github.com/sebastianbergmann/phpcpd/tree
 * @since     Class available since Release 1.0.0
 */
class Xml extends AbstractXmlLogger implements ReportInterface
{
    /**
     * Processes a list of clones.
     *
     * @param array $clones
     */
    public function processClones(array $clones)
    {
        $cpd = $this->document->createElement('pmd-cpd');
        $this->document->appendChild($cpd);

       if (isset($clones['clones'])) {
           foreach ($clones['clones'] as $clone) {
               $duplication = $cpd->appendChild(
                   $this->document->createElement('duplication')
               );

               $duplication->setAttribute('lines', $clone['linesCount']);
               $duplication->setAttribute('tokens', $clone['tokenCount']);

               foreach ($clone['files'] as $codeCloneFile) {
                   $file = $duplication->appendChild(
                       $this->document->createElement('file')
                   );

                   $file->setAttribute('path', $codeCloneFile['name']);
                   $file->setAttribute('line', $codeCloneFile['startLine']);
               }
           }
       }

        $this->flush();
    }
}
