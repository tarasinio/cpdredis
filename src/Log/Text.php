<?php
/*
 * This file is part of PHP Copy/Paste Detector (PHPCPD).
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\PHPCPD\Log;

use SebastianBergmann\PHPCPD\CodeCloneMap;
use SebastianBergmann\PHPCPD\CodeClone;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * A ResultPrinter for the TextUI.
 *
 * @author    Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @license   http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link      http://github.com/sebastianbergmann/phpcpd/tree
 * @since     Class available since Release 2.0.0
 */
class Text
{
    /**
     * @param OutputInterface $output
     * @param array $resultArray
     */
    public function printResult(OutputInterface $output,array $resultArray = array())
    {
        $output->write(
            sprintf(
                "SUM Duplicate token count is %s.\n\n",
                $resultArray['sumTokenCount']
            )
        );
        if (isset($resultArray['clones'])) {
            $output->write("Clones:\n\n");
            foreach ($resultArray['clones'] as $clone) {
                $output->write(sprintf("      duplicate token count is %s.\n", $clone['tokenCount']));
                $output->write(sprintf("      lines count is %s.\n", $clone['linesCount']));
                $output->write("      files:\n");
                foreach ($clone['files'] as $file) {
                    $output->write('                ' . $file['name'] . ':' . $file['startLine'] . "\n");
                }
                $output->write("\n");
            }
        }
    }
}
