<?php
/**
 * Created by PhpStorm.
 * User: ydmytrunets
 * Date: 8/25/2015
 * Time: 4:45 PM
 */

namespace SebastianBergmann\PHPCPD\Log;


interface ReportInterface
{
    /**
     * Processes a list of clones.
     *
     * @param array $clones
     */
    public function processClones(array $clones);
} 
