<?php
/*
 * This file is part of PHP Copy/Paste Detector (PHPCPD).
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\PHPCPD\CLI;

use SebastianBergmann\PHPCPD\Detector\Detector;
use SebastianBergmann\PHPCPD\Detector\Strategy\ConnectStrategy;
use SebastianBergmann\PHPCPD\Detector\Strategy\DefaultStrategy;
use SebastianBergmann\PHPCPD\Detector\Strategy\ReindexStrategy;
use SebastianBergmann\PHPCPD\Log\Reporting;
use SebastianBergmann\PHPCPD\Log\Text;
use SebastianBergmann\FinderFacade\FinderFacade;
use Symfony\Component\Console\Command\Command as AbstractCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * @author    Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @license   http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link      http://github.com/sebastianbergmann/phpcpd/tree
 * @since     Class available since Release 2.0.0
 */
class Command extends AbstractCommand
{
    /**
     * @var array
     */
    protected $ignorePaths = array();

    /**
     * Configures the current command.
     */
    protected function configure()
    {
        $this->setName('phpcpd')
             ->setDefinition(
                 array(
                     new InputArgument(
                         'values',
                         InputArgument::IS_ARRAY,
                         'Files and directories to analyze'
                     )
                 )
             )
             ->addOption(
                 'names',
                 null,
                 InputOption::VALUE_REQUIRED,
                 'A comma-separated list of file names to check',
                 array('*.php')
             )
             ->addOption(
                 'names-exclude',
                 null,
                 InputOption::VALUE_REQUIRED,
                 'A comma-separated list of file names to exclude',
                 array()
             )
             ->addOption(
                 'exclude',
                 null,
                 InputOption::VALUE_REQUIRED | InputOption::VALUE_IS_ARRAY,
                 'Exclude a directory from code analysis (must be relative to source)'
             )
             ->addOption(
                 'log-file',
                 null,
                 InputOption::VALUE_REQUIRED,
                 'Write result in PMD-CPD XML format to file'
             )
             ->addOption(
                 'min-lines',
                 null,
                 InputOption::VALUE_REQUIRED,
                 'Minimum number of identical lines',
                 5
             )
             ->addOption(
                 'min-tokens',
                 null,
                 InputOption::VALUE_REQUIRED,
                 'Minimum number of identical tokens',
                 70
             )
             ->addOption(
                 'fuzzy',
                 null,
                 InputOption::VALUE_NONE,
                 'Fuzz variable names'
             )
             ->addOption(
                 'progress',
                 null,
                 InputOption::VALUE_NONE,
                 'Show progress bar'
             )
            ->addOption(
                 'index',
                 null,
                 InputOption::VALUE_NONE,
                 'Make index of folder'
             )
            ->addOption(
                'redis_host',
                null,
                InputOption::VALUE_REQUIRED,
                'Redis host'
            )
            ->addOption(
                'redis_port',
                null,
                InputOption::VALUE_REQUIRED,
                'Redis port'
            )
            ->addOption(
                'redis_password',
                null,
                InputOption::VALUE_REQUIRED,
                'Redis password'
            )
            ->addOption(
                'report_format',
                null,
                InputOption::VALUE_REQUIRED,
                'Format output data for report',
                'xml'
            )
            ->addOption(
                'plagiarism-mode-disable',
                null,
                InputOption::VALUE_NONE,
                'Plagiarism mode for compare with tokens in Redis'
            )
            ->addOption(
                'vendor_name',
                null,
                InputOption::VALUE_REQUIRED,
                'Vendor name of package'
            )
            ->addOption(
                'package_name',
                null,
                InputOption::VALUE_REQUIRED,
                'Package name of package'
            )
            ->addOption(
                'package_version',
                null,
                InputOption::VALUE_REQUIRED,
                'Version of package'
            )
            ->addOption(
                'uuid',
                null,
                InputOption::VALUE_REQUIRED,
                'unique id of build'
            )
            ->addOption(
                'mage_account_id',
                null,
                InputOption::VALUE_REQUIRED,
                'Mage account ID'
            )
            ->addOption(
                'phptools_ignorelist_file',
                null,
                InputOption::VALUE_REQUIRED,
                'Path to file with ignore list'
            )
        ;
    }

    /**
     * Executes the current command.
     *
     * @param InputInterface  $input  An InputInterface instance
     * @param OutputInterface $output An OutputInterface instance
     *
     * @return null|integer null or 0 if everything went fine, or an error code
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ((null !== ($ignorelistFile = $input->getOption('phptools_ignorelist_file')))
            && file_exists($ignorelistFile)) {

            if (!($xmlObject = simplexml_load_file($ignorelistFile))) {
                $errors = libxml_get_errors();
                foreach ($errors as $error) {
                    $output->writeln($error);
                }
            } else {
                $ignoreFiles = $xmlObject->xpath('*/pathPart');
                foreach ($ignoreFiles as $ignoreFile) {
                    /** @var \SimpleXMLElement $ignoreFile */
                    $this->ignorePaths[] = (string)$ignoreFile;
                }
            }
        }
        $finder = new FinderFacade(
            $input->getArgument('values'),
            $input->getOption('exclude'),
            $this->handleCSVOption($input, 'names'),
            $this->handleCSVOption($input, 'names-exclude')
        );
        $vendorName = $input->getOption('vendor_name');
        $packageName = $input->getOption('package_name');
        $uuid = $input->getOption('uuid');
        $mageAccountId = $input->getOption('mage_account_id');
        $packageVersion = $input->getOption('package_version');
        if (in_array(null, array($vendorName ,$packageName, $packageVersion, $uuid, $mageAccountId))) {
            $output->writeln('Enter package name, vendor, version, uuid, mageAccountId');
            exit(1);
        }
        $files = $finder->findFiles();

        if (!empty($this->ignorePaths)) {
            $files = $this->excludeIgnoredLibraries($files);
        }
        if (empty($files)) {
            $output->writeln('No files found to scan');
            exit(1);
        }

        $progressHelper = null;

        if ($input->getOption('progress')) {
            $progressHelper = $this->getHelperSet()->get('progress');
            $progressHelper->start($output, count($files));
        }
        $redisConfig = array(
            'host' => $input->getOption('redis_host'),
            'port' => $input->getOption('redis_port')
        );
        if ($input->getOption('redis_password'))
        {
            $redisConfig['password'] = $input->getOption('redis_password');
        }

        $indexStrategy = new ReindexStrategy($redisConfig, $vendorName, $packageName, $packageVersion, $uuid, $mageAccountId);
        if ($input->getOption('plagiarism-mode-disable')) {
            $strategy = new DefaultStrategy();
        } else {
            $strategy = new ConnectStrategy($redisConfig, $vendorName, $packageName, $packageVersion, $uuid, $mageAccountId);
        }
        $detector = new Detector($strategy, $progressHelper, $indexStrategy);
        $quiet    = $output->getVerbosity() == OutputInterface::VERBOSITY_QUIET;

        if ($input->getOption('index')) {
            $detector->index(
                $files,
                $input->getOption('min-lines'),
                $input->getOption('min-tokens'),
                $input->getOption('fuzzy')
            );
            print('Indexing is completed');
            return;
        }
        $clones = $detector->copyPasteDetection(
            $files,
            $input->getOption('min-lines'),
            $input->getOption('min-tokens'),
            $input->getOption('fuzzy')
        );

        if ($input->getOption('progress')) {
            $progressHelper->finish();
            $output->writeln('');
        }

        if (!$quiet) {
            $printer = new Text;
            $printer->printResult($output, $clones);
            unset($printer);
        }

        $reportFile = $input->getOption('log-file');

        if ($reportFile) {
            $reporting = new Reporting();
            $reporting->printReport($input->getOption('report_format'), $reportFile, $clones);
            unset($reporting);
        }

        if (!$quiet) {
            print \PHP_Timer::resourceUsage() . "\n";
        }

        if (count($clones) > 0) {
            exit(1);
        }
    }

    /**
     * @param  \Symfony\Component\Console\Input\InputOption $input
     * @param  string                                      $option
     * @return array
     */
    private function handleCSVOption(InputInterface $input, $option)
    {
        $result = $input->getOption($option);

        if (!is_array($result)) {
            $result = explode(',', $result);
            array_map('trim', $result);
        }

        return $result;
    }

    /**
     * @param string $name
     * @param array $ignoreList
     * @return bool
     */
    protected function isFromIgnoreList($name, array $ignoreList)
    {
        foreach ($ignoreList as $ignorePath) {
            if (strpos(strtoupper($name), strtoupper($ignorePath)) !== false) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param array $files
     * @return array
     */
    protected function excludeIgnoredLibraries(array $files)
    {
        $filteredFiles = array();
        foreach ($files as $name) {
            if (!$this->isFromIgnoreList($name, $this->ignorePaths)) {
                $filteredFiles[] = $name;
            }
        }
        return $filteredFiles;
    }
}
