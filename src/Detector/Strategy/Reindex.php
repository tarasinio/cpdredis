<?php
/*
 * This file is part of PHP Copy/Paste Detector (PHPCPD).
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\PHPCPD\Detector\Strategy;

use SebastianBergmann\PHPCPD\CodeCloneMap;

/**
 * Default strategy for detecting code clones.
 *
 * @author    Johann-Peter Hartmann <johann-peter.hartmann@mayflower.de>
 * @author    Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @license   http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link      http://github.com/sebastianbergmann/phpcpd/tree
 * @since     Class available since Release 1.4.0
 */
class ReindexStrategy extends AbstractStrategy
{

    /**
     * @var \Predis\Client
     */
    protected $predis;

    public $vendorName;

    public $packageName;

    public $packageVersion;

    public $uuid;

    public $mageAccountId;

    public function __construct($redisConfig, $vendorName, $packageName, $packageVersion,$uuid = 'uuid', $mageAccountId = 'nomageaccountid')
    {
        $this->predis = new \Predis\Client($redisConfig);
        $this->vendorName = $vendorName;
        $this->packageName = $packageName;
        $this->packageVersion = $packageVersion;
        $this->uuid = $uuid;
        $this->mageAccountId = $mageAccountId;
    }

    /**
     * Copy & Paste Detection (CPD).
     *
     * @param  string $file
     * @param  integer $minLines
     * @param  integer $minTokens
     * @param  CodeCloneMap $result
     * @param  boolean $fuzzy
     * @author Johann-Peter Hartmann <johann-peter.hartmann@mayflower.de>
     */
    public function processFile($file, $minLines, $minTokens, CodeCloneMap $result, $fuzzy = false)
    {
        $buffer = file_get_contents($file);
        $currentTokenPositions = array();
        $currentTokenRealPositions = array();
        $currentSignature = '';
        $tokens = token_get_all($buffer);
        $tokenNr = 0;
        $lastTokenLine = 0;

        $savingFilePath = strstr($file,"{$this->vendorName}_{$this->packageName}_{$this->packageVersion}_{$this->uuid}");
        $savingFilePath = str_replace("{$this->vendorName}_{$this->packageName}_{$this->packageVersion}_{$this->uuid}", "{$this->vendorName}_{$this->packageName}_{$this->packageVersion}", $savingFilePath);

        $result->setNumLines(
            $result->getNumLines() + substr_count($buffer, "\n")
        );

        unset($buffer);

        foreach (array_keys($tokens) as $key) {
            $token = $tokens[$key];

            if (is_array($token)) {
                if (!isset($this->tokensIgnoreList[$token[0]])) {
                    if ($tokenNr == 0) {
                        $currentTokenPositions[$tokenNr] = $token[2] - $lastTokenLine;
                    } else {
                        $currentTokenPositions[$tokenNr] = $currentTokenPositions[$tokenNr - 1] +
                            $token[2] - $lastTokenLine;
                    }

                    $currentTokenRealPositions[$tokenNr++] = $token[2];

                    if ($fuzzy && $token[0] == T_VARIABLE) {
                        $token[1] = 'variable';
                    }

                    $currentSignature .= chr($token[0] & 255) .
                        pack('N*', crc32($token[1]));
                }

                $lastTokenLine = $token[2];
            }
        }

        $count = count($currentTokenPositions);
        $firstLine = 0;
        $firstRealLine = 0;
        $found = false;
        $tokenNr = 0;

        while ($tokenNr <= $count - $minTokens) {
            $line = $currentTokenPositions[$tokenNr];
            $realLine = $currentTokenRealPositions[$tokenNr];

            $hash = substr(
                md5(
                    substr(
                        $currentSignature,
                        $tokenNr * 5,
                        $minTokens * 5
                    ),
                    true
                ),
                0,
                8
            );
            $hashFromRedisSerialized = $this->predis->get($hash);
            $hashFromRedis = unserialize(base64_decode($hashFromRedisSerialized));
            if ($hashFromRedis) {
                $found = true;
                if ($firstLine === 0) {
                    $firstLine = $line;
                    $firstHash = $hash;
                }
            } else {
                if ($found) {
                    $hashFromRedisSerialized = $this->predis->get($firstHash);
                    $hashFromRedis = unserialize(base64_decode($hashFromRedisSerialized));
                    $found = false;
                    $firstLine = 0;
                }
            }
            if ($hashFromRedis) {
                $info = $hashFromRedis;
                $info[$savingFilePath] = array($savingFilePath, $realLine, $this->vendorName, $this->mageAccountId);
            } else {
                $info = array($savingFilePath => array($savingFilePath, $realLine, $this->vendorName, $this->mageAccountId));
            }
            $serialized = base64_encode(serialize($info));
            $this->predis->set($hash, $serialized);
            $tokenNr++;
        }
        if ($found) {
            $found = false;
        }
    }
}
