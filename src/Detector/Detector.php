<?php
/*
 * This file is part of PHP Copy/Paste Detector (PHPCPD).
 *
 * (c) Sebastian Bergmann <sebastian@phpunit.de>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace SebastianBergmann\PHPCPD\Detector;

use SebastianBergmann\PHPCPD\Detector\Strategy\AbstractStrategy;
use SebastianBergmann\PHPCPD\CodeCloneMap;
use Symfony\Component\Console\Helper\ProgressHelper;

/**
 * PHPCPD code analyser.
 *
 * @author    Johann-Peter Hartmann <johann-peter.hartmann@mayflower.de>
 * @author    Sebastian Bergmann <sebastian@phpunit.de>
 * @copyright Sebastian Bergmann <sebastian@phpunit.de>
 * @license   http://www.opensource.org/licenses/BSD-3-Clause  The BSD 3-Clause License
 * @link      http://github.com/sebastianbergmann/phpcpd/tree
 * @since     Class available since Release 1.0.0
 */
class Detector
{
    /**
     * @var SebastianBergmann\PHPCPD\Detector\Strategy\AbstractStrategy
     */
    protected $strategy;

    /**
     * @var Symfony\Component\Console\Helper\ProgressHelper
     */
    protected $progressHelper;

    /**
     * @var Indexer $indexer
     */
    protected $indexer;

    /**
     * Constructor.
     *
     * @param AbstractStrategy $strategy
     * @since Method available since Release 1.3.0
     */
    public function __construct(AbstractStrategy $strategy, ProgressHelper $progressHelper = null, $indexStrategy)
    {
        $this->strategy       = $strategy;
        $this->progressHelper = $progressHelper;
        $this->indexer = $indexStrategy;
    }


    public function index($files, $minLines = 5, $minTokens = 70, $fuzzy = false)
    {
        $result = new CodeCloneMap;

        foreach ($files as $file) {
            $this->indexer->processFile(
                $file,
                $minLines,
                $minTokens,
                $result,
                $fuzzy
            );

            if ($this->progressHelper !== null) {
                $this->progressHelper->advance();
            }
        }
        return true;
    }
    /**
     * Copy & Paste Detection (CPD).
     *
     * @param  Iterator|array $files     List of files to process
     * @param  integer        $minLines  Minimum number of identical lines
     * @param  integer        $minTokens Minimum number of identical tokens
     * @param  boolean        $fuzzy
     * @return CodeCloneMap   Map of exact clones found in the list of files
     */
    public function copyPasteDetection($files, $minLines = 5, $minTokens = 70, $fuzzy = false)
    {
        $result = new CodeCloneMap;

        foreach ($files as $file) {
            $this->strategy->processFile(
                $file,
                $minLines,
                $minTokens,
                $result,
                $fuzzy
            );

            if ($this->progressHelper !== null) {
                $this->progressHelper->advance();
            }
        }

        $resultInfo = array();
        $resultInfo['sumTokenCount'] = 0;

        foreach ($result->getClones() as $clone)
        {
            $resultArray['tokenCount'] = $clone->getTokens();
            $resultArray['linesCount'] = $clone->getSize();
            $resultInfo['sumTokenCount'] +=$clone->getTokens();
            $files = array();
            foreach ($clone->getFiles() as $cloneFile) {
                $reportFilePath = $this->getReportFilePath($cloneFile);
                $files[] = array(
                    'name' => $reportFilePath ?: $cloneFile->getName(),
                    'startLine' => $cloneFile->getStartLine()
                );
            }
            $resultArray['files'] = $files;
            $resultInfo['clones'][] = $resultArray;
        }
        return $resultInfo;
    }

    /**
     * @param \SebastianBergmann\PHPCPD\CodeCloneFile $cloneFile
     * @return string
     */
    public function getReportFilePath($cloneFile)
    {
        $reportFilePath = strstr($cloneFile->getName(), "{$this->strategy->vendorName}_{$this->strategy->packageName}_{$this->strategy->packageVersion}_{$this->strategy->uuid}");
        $reportFilePath = str_replace(
            "{$this->strategy->vendorName}_{$this->strategy->packageName}_{$this->strategy->packageVersion}_{$this->strategy->uuid}",
            "{$this->strategy->vendorName}_{$this->strategy->packageName}_{$this->strategy->packageVersion}",
            $reportFilePath);
        return $reportFilePath;
    }
}
